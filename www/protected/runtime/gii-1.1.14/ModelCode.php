<?php
return array (
  'template' => 'phpDoc',
  'connectionId' => 'db',
  'tablePrefix' => 'faraon_',
  'modelPath' => 'application.modules.lady.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '1',
);
