<?php

class m131003_173358_install extends CDbMigration {

    public function up() {
        $this->createTable("{{text}}", array(
            'id'      => 'pk',
            'title'   => 'varchar(128) not null comment "Название"',
            'before'  => 'varchar(2048) not null comment "Краткое описание"',
            'content' => 'text null comment "Описание"',
            'status'  => 'varchar(64) not null comment "Статус"',
        ));
        $this->createTable("{{service}}", array(
            'id'         => 'pk',
            'title'      => 'varchar(128) not null comment "Название"',
            'before'     => 'varchar(2048) not null comment "Краткое описание"',
            'content'    => 'text null comment "Описание"',
            'status'     => 'varchar(64) not null comment "Статус"',
            'price'      => 'float not null comment "Цена"',
            'unit'       => 'varchar(128) not null comment "Ед.из"',
            'in_widgets' => 'int not null comment "Использовать для слайдера"',
        ));
        $this->createTable("{{articul}}", array(
            'id'      => 'pk',
            'title'   => 'varchar(128) not null comment "Название"',
            'before'  => 'varchar(2048) not null comment "Краткое описание"',
            'content' => 'text null comment "Описание"',
            'status'  => 'varchar(64) not null comment "Статус"',
        ));
        $this->createTable("{{lady}}", array(
            'id'      => 'pk',
            'name'    => 'varchar(256) not null comment "Имя"',
            'before'  => 'varchar(2048) not null comment "Краткое описание"',
            'content' => 'text null comment "Описание"',
            'status'  => 'varchar(64) not null comment "Статус"',
        ));
        $this->createTable("{{lady_service}}", array(
            'id'         => 'pk',
            'lady_id'    => 'int not null',
            'service_id' => 'int not null',
            'KEY lady_id(lady_id)',
            'KEY service_id(service_id)',
        ));
        $this->createTable("{{service_request}}", array(
            'id'              => 'pk',
            'start_time'      => 'int not null comment "Начало"',
            'long_time'       => 'int not null comment "Длительность"',
            'description'     => 'text null comment "Комментарий"',
            'lady_service_id' => 'int not null',
            'KEY lady_service_id(lady_service_id)',
        ));
        $this->createTable("{{lady_worktime}}", array(
            'id'          => 'pk',
            'start_time'  => 'int not null comment "Начало"',
            'finish_time' => 'int not null comment "Конец"',
            'lady_id'     => 'int not null',
            'KEY lady_id(lady_id)',
        ));
        $this->insert("{{text}}", array(
            'title'   => 'Главная',
            'before'  => 'Краткое описание главной',
            'content' => 'Полное описание главной',
            'status'  => 'ok',
        ));
    }

    public function down() {
        $this->dropTable("{{text}}");
        $this->dropTable("{{service}}");
        $this->dropTable("{{articul}}");
        $this->dropTable("{{lady}}");
        $this->dropTable("{{lady_service}}");
        $this->dropTable("{{service_request}}");
        $this->dropTable('{{lady_worktime}}');
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}

