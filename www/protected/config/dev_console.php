<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'       => 'Faraon-vip',
    // preloading 'log' component
    'preload'    => array('log'),
    // application components
    'components' => array(
        'db'  => array(
            'connectionString' => 'mysql:host=localhost;dbname=gp_faraon',
            'emulatePrepare'   => true,
            'username'         => 'root',
            'password'         => '132435',
            'charset'          => 'utf8',
            'tablePrefix'      => 'faraon_'
        ),
        'log' => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
);