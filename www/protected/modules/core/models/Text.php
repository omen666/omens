<?php

/**
 * This is the model class for table "{{text}}".
 *
 * The followings are the available columns in table '{{text}}':
 * @property integer $id
 * @property string $title
 * @property string $before
 * @property string $content
 * @property string $status
 * Доки для удобного автокомплита по методам
 * @method Text    cache()                 cache($duration, $dependency=null, $queryCount=1)
 * @method Text    find()                  find($condition='', $params=array())
 * @method Text[]  findAll()               findAll($condition='', $params=array())
 * @method Text[]  findAllByPk()           findAllByPk($pk,$condition='', $params=array())
 * @method Text[]  findAllByAttributes()   findAllByAttributes($attributes,$condition='',$params=array())
 * @method Text    findByAttributes()      findByAttributes($attributes,$condition='',$params=array())
 * @method Text    findByPk()              findByPk($pk,$condition='', $params=array())
 * @method Text    findBySql()             findBySql($sql,$params=array())
 * @method Text[]  populateRecords()       populateRecords($data,$callAfterFind=true,$index=null)
 * @method Text    resetScope()            resetScope($resetDefault=true)
 * @method Text    together()              together()
 * @method Text    with()                  with()
 */
class Text extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Text the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{text}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, before, status', 'required'),
			array('title', 'length', 'max'=>128),
			array('before', 'length', 'max'=>2048),
			array('status', 'length', 'max'=>64),
			array('content', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, before, content, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'before' => 'Краткое описание',
			'content' => 'Описание',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('before',$this->before,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}