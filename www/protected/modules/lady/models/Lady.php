<?php

/**
 * This is the model class for table "{{lady}}".
 *
 * The followings are the available columns in table '{{lady}}':
 * @property integer $id
 * @property string $name
 * @property string $before
 * @property string $content
 * @property string $status
 * Доки для удобного автокомплита по методам
 * @method Lady    cache()                 cache($duration, $dependency=null, $queryCount=1)
 * @method Lady    find()                  find($condition='', $params=array())
 * @method Lady[]  findAll()               findAll($condition='', $params=array())
 * @method Lady[]  findAllByPk()           findAllByPk($pk,$condition='', $params=array())
 * @method Lady[]  findAllByAttributes()   findAllByAttributes($attributes,$condition='',$params=array())
 * @method Lady    findByAttributes()      findByAttributes($attributes,$condition='',$params=array())
 * @method Lady    findByPk()              findByPk($pk,$condition='', $params=array())
 * @method Lady    findBySql()             findBySql($sql,$params=array())
 * @method Lady[]  populateRecords()       populateRecords($data,$callAfterFind=true,$index=null)
 * @method Lady    resetScope()            resetScope($resetDefault=true)
 * @method Lady    together()              together()
 * @method Lady    with()                  with()
 */
class Lady extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Lady the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{lady}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, before, status', 'required'),
            array('name', 'length', 'max' => 256),
            array('before', 'length', 'max' => 2048),
            array('status', 'length', 'max' => 64),
            array('content', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, before, content, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lady_service'  => array(self::HAS_MANY, 'LadyService', 'lady_id'),
            'lady_worktime' => array(self::HAS_MANY, 'LadyWorktime', 'lady_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'      => 'ID',
            'name'    => 'Имя',
            'before'  => 'Краткое описание',
            'content' => 'Описание',
            'status'  => 'Статус',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('before', $this->before, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('status', $this->status, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}

