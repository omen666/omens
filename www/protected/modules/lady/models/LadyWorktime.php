<?php

/**
 * This is the model class for table "{{lady_worktime}}".
 *
 * The followings are the available columns in table '{{lady_worktime}}':
 * @property integer $id
 * @property integer $start_time
 * @property integer $finish_time
 * @property integer $lady_id
 * Доки для удобного автокомплита по методам
 * @method LadyWorktime    cache()                 cache($duration, $dependency=null, $queryCount=1)
 * @method LadyWorktime    find()                  find($condition='', $params=array())
 * @method LadyWorktime[]  findAll()               findAll($condition='', $params=array())
 * @method LadyWorktime[]  findAllByPk()           findAllByPk($pk,$condition='', $params=array())
 * @method LadyWorktime[]  findAllByAttributes()   findAllByAttributes($attributes,$condition='',$params=array())
 * @method LadyWorktime    findByAttributes()      findByAttributes($attributes,$condition='',$params=array())
 * @method LadyWorktime    findByPk()              findByPk($pk,$condition='', $params=array())
 * @method LadyWorktime    findBySql()             findBySql($sql,$params=array())
 * @method LadyWorktime[]  populateRecords()       populateRecords($data,$callAfterFind=true,$index=null)
 * @method LadyWorktime    resetScope()            resetScope($resetDefault=true)
 * @method LadyWorktime    together()              together()
 * @method LadyWorktime    with()                  with()
 */
class LadyWorktime extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LadyWorktime the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{lady_worktime}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('start_time, finish_time, lady_id', 'required'),
            array('start_time, finish_time, lady_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, start_time, finish_time, lady_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lady' => array(self::BELONGS_TO, 'Lady', 'lady_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'          => 'ID',
            'start_time'  => 'Начало',
            'finish_time' => 'Конец',
            'lady_id'     => 'Lady',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('start_time', $this->start_time);
        $criteria->compare('finish_time', $this->finish_time);
        $criteria->compare('lady_id', $this->lady_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}

