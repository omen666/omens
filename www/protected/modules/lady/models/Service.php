<?php

/**
 * This is the model class for table "{{service}}".
 *
 * The followings are the available columns in table '{{service}}':
 * @property integer $id
 * @property string $title
 * @property string $before
 * @property string $content
 * @property string $status
 * @property double $price
 * @property string $unit
 * @property integer $in_widgets
 * Доки для удобного автокомплита по методам
 * @method Service    cache()                 cache($duration, $dependency=null, $queryCount=1)
 * @method Service    find()                  find($condition='', $params=array())
 * @method Service[]  findAll()               findAll($condition='', $params=array())
 * @method Service[]  findAllByPk()           findAllByPk($pk,$condition='', $params=array())
 * @method Service[]  findAllByAttributes()   findAllByAttributes($attributes,$condition='',$params=array())
 * @method Service    findByAttributes()      findByAttributes($attributes,$condition='',$params=array())
 * @method Service    findByPk()              findByPk($pk,$condition='', $params=array())
 * @method Service    findBySql()             findBySql($sql,$params=array())
 * @method Service[]  populateRecords()       populateRecords($data,$callAfterFind=true,$index=null)
 * @method Service    resetScope()            resetScope($resetDefault=true)
 * @method Service    together()              together()
 * @method Service    with()                  with()
 */
class Service extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Service the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{service}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, before, status, price, unit, in_widgets', 'required'),
            array('in_widgets', 'numerical', 'integerOnly' => true),
            array('price', 'numerical'),
            array('title, unit', 'length', 'max' => 128),
            array('before', 'length', 'max' => 2048),
            array('status', 'length', 'max' => 64),
            array('content', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, before, content, status, price, unit, in_widgets', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lady_service' => array(self::HAS_MANY, 'LadyService', 'service_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'         => 'ID',
            'title'      => 'Название',
            'before'     => 'Краткое описание',
            'content'    => 'Описание',
            'status'     => 'Статус',
            'price'      => 'Цена',
            'unit'       => 'Ед.из',
            'in_widgets' => 'Использовать для слайдера',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('before', $this->before, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('price', $this->price);
        $criteria->compare('unit', $this->unit, true);
        $criteria->compare('in_widgets', $this->in_widgets);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}

