<?php

/**
 * This is the model class for table "{{service_request}}".
 *
 * The followings are the available columns in table '{{service_request}}':
 * @property integer $id
 * @property integer $start_time
 * @property integer $long_time
 * @property string $description
 * @property integer $lady_service_id
 * Доки для удобного автокомплита по методам
 * @method ServiceRequest    cache()                 cache($duration, $dependency=null, $queryCount=1)
 * @method ServiceRequest    find()                  find($condition='', $params=array())
 * @method ServiceRequest[]  findAll()               findAll($condition='', $params=array())
 * @method ServiceRequest[]  findAllByPk()           findAllByPk($pk,$condition='', $params=array())
 * @method ServiceRequest[]  findAllByAttributes()   findAllByAttributes($attributes,$condition='',$params=array())
 * @method ServiceRequest    findByAttributes()      findByAttributes($attributes,$condition='',$params=array())
 * @method ServiceRequest    findByPk()              findByPk($pk,$condition='', $params=array())
 * @method ServiceRequest    findBySql()             findBySql($sql,$params=array())
 * @method ServiceRequest[]  populateRecords()       populateRecords($data,$callAfterFind=true,$index=null)
 * @method ServiceRequest    resetScope()            resetScope($resetDefault=true)
 * @method ServiceRequest    together()              together()
 * @method ServiceRequest    with()                  with()
 */
class ServiceRequest extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ServiceRequest the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{service_request}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('start_time, long_time, lady_service_id', 'required'),
            array('start_time, long_time, lady_service_id', 'numerical', 'integerOnly' => true),
            array('description', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, start_time, long_time, description, lady_service_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'service_request' => array(self::BELONGS_TO, 'LadyService', 'ledy_service_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'              => 'ID',
            'start_time'      => 'Начало',
            'long_time'       => 'Длительность',
            'description'     => 'Комментарий',
            'lady_service_id' => 'Lady Service',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('start_time', $this->start_time);
        $criteria->compare('long_time', $this->long_time);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('lady_service_id', $this->lady_service_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}

