<?php

/**
 * This is the model class for table "{{lady_service}}".
 *
 * The followings are the available columns in table '{{lady_service}}':
 * @property integer $id
 * @property integer $lady_id
 * @property integer $service_id
 * Доки для удобного автокомплита по методам
 * @method LadyService    cache()                 cache($duration, $dependency=null, $queryCount=1)
 * @method LadyService    find()                  find($condition='', $params=array())
 * @method LadyService[]  findAll()               findAll($condition='', $params=array())
 * @method LadyService[]  findAllByPk()           findAllByPk($pk,$condition='', $params=array())
 * @method LadyService[]  findAllByAttributes()   findAllByAttributes($attributes,$condition='',$params=array())
 * @method LadyService    findByAttributes()      findByAttributes($attributes,$condition='',$params=array())
 * @method LadyService    findByPk()              findByPk($pk,$condition='', $params=array())
 * @method LadyService    findBySql()             findBySql($sql,$params=array())
 * @method LadyService[]  populateRecords()       populateRecords($data,$callAfterFind=true,$index=null)
 * @method LadyService    resetScope()            resetScope($resetDefault=true)
 * @method LadyService    together()              together()
 * @method LadyService    with()                  with()
 */
class LadyService extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LadyService the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{lady_service}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lady_id, service_id', 'required'),
            array('lady_id, service_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, lady_id, service_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lady'            => array(self::BELONGS_TO, 'Lady', 'lady_id'),
            'service'         => array(self::BELONGS_TO, 'Service', 'service_id'),
            'service_request' => array(self::HAS_MANY, 'ServiceRequest', 'ledy_service_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'         => 'ID',
            'lady_id'    => 'Lady',
            'service_id' => 'Service',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('lady_id', $this->lady_id);
        $criteria->compare('service_id', $this->service_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}

